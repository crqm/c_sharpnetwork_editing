﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;

namespace WNet
{
    public partial class Form3 : Form
    {
        public Form3()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //实例化IPEndPoint类对象
            IPEndPoint iPEndPoint = new IPEndPoint(IPAddress.Parse(textBox1.Text), 80);
            //使用IPEndPoint类对象获取终结点的IP地址和端口号
            label2.Text = "IP地址：" + iPEndPoint.Address.ToString() + "\n端口号：" + iPEndPoint.Port;
        }
    }
}
