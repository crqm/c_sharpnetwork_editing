﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;

namespace WNet
{
    public partial class Form7 : Form
    {
        public Form7()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            TcpListener tcpListener = null;
            IPAddress iPAddress = IPAddress.Parse(textBox1.Text);
            int port = Convert.ToInt32(textBox2.Text);
            tcpListener = new TcpListener(iPAddress, port);
            tcpListener.Start();
            richTextBox1.Text = "等待连接...\n";
            TcpClient tcpClient = null;
            if (tcpListener.Pending())
            {
                tcpClient = tcpListener.AcceptTcpClient();
            }
            else
            {
                tcpClient = new TcpClient(textBox1.Text, port);
            }
            richTextBox1.Text += "连接成功!\n";
            tcpClient.Close();
            tcpListener.Stop();
        }
    }
}
