﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Net;
using System.Net.Sockets;

namespace WNet
{
    public partial class Form4 : Form
    {
        public Form4()
        {
            InitializeComponent();
        }
        //获取网页内容
        private void button1_Click(object sender, EventArgs e)
        {
            richTextBox1.Text = string.Empty;
            WebClient wclient = new WebClient();//实例化WebClient类对象
            wclient.BaseAddress = textBox1.Text;//设置WebClient的基的URI
            wclient.Encoding = Encoding.UTF8;//指定下载字符串的编码方式
            //为WebClient类对象添加报头
            wclient.Headers.Add("Content-Type", "application/x-www-form-urlencoded");
            //使用StreamReader声明一个流读取变量sreader
            Stream stream = wclient.OpenRead(textBox1.Text);
            string str = string.Empty;//声明一个变量，用来保存一行从WebClient下载的数据
            StreamReader sreader = new StreamReader(stream);
            //循环读取从指定网站获得的数据
            while ((str = sreader.ReadLine())!= null){
                richTextBox1.Text += str + "\n";
            }
            //调用WebClient对象的DownloadFile方法将指定网站的内容保存在文件中
            wclient.DownloadFile(textBox1.Text, DateTime.Now.ToFileTime() + ".txt");
            MessageBox.Show("保存文件成功");
        }
    }
}
