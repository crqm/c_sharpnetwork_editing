﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;

namespace WNet
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            label2.Text = string.Empty;//初始化label标签
            IPAddress[] ips = Dns.GetHostAddresses(textBox1.Text);//获取指定主机的IP地址族
            foreach(IPAddress ip in ips)
            {
                //在label标签中显示得到的IP地址信息
                label2.Text = "网际协议地址：" + ip.Address + "\nIP地址的地址族：" + ip.AddressFamily.ToString() + "\n是否IPV6链接本地地址：" + ip.IsIPv6LinkLocal;
            }
        }
    }
}
